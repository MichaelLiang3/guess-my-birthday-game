from random import randint

name = input("Hi! What is your name?")
guess_number = 0

for guess_num in range(5):

    guess_number += 1
    month_guess = randint(1,12)
    year_guess = randint(1924,2004)
    print("Guess", guess_number, ":", name, "were you born in", month_guess, "/", year_guess, "?")
    response = input("yes or no?")


    if response == "yes":
        print("I knew it!")
        exit()

    elif response == "no" and guess_number < 5:
        print("Drat! Lemme try again!")

    else:
        print("I have other things to do. Good bye.")
